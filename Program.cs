﻿using System;
using System.Linq;
using System.Text;
using System.IO;

namespace Twist
{
    class Program
    {
        static void Main()
        {

            Console.WriteLine("Language Twister!");
            Console.WriteLine();
            Console.WriteLine("1. Twist a word");
            Console.WriteLine("2. Read a twisted word");
            Console.WriteLine("3. Twist a phrase");
            Console.WriteLine("4. Read a twisted phrase");
            Console.WriteLine("5. Exit program");
            Console.WriteLine();

            string userChoice = Console.ReadLine();
            string input;
            char checkSpace = ' ';

            switch (userChoice)
            {
                case "1":
                    Console.WriteLine("Please enter a word");
                    input = Console.ReadLine();

                    if (input.Contains(checkSpace) == true)
                    {
                        Console.WriteLine("Please enter only a single word for this function.");
                    }
                    if (input.Length < 4)
                    {
                        Console.WriteLine("Please only enter words with 4 or more characters");
                        Main();
                    }
                    if (input[1] == input[2])
                    {
                        Console.WriteLine("Words with identical characters in the middle can not be twisted");
                    }
                    else
                    {
                        Console.WriteLine("Twisted word: " + Twist(input));
                    }
                    break;
                case "2":
                    Console.WriteLine("Please enter a twisted word:");
                    input = Console.ReadLine();
                    if (input.Contains(checkSpace) == true)
                    {
                        Console.WriteLine("Please enter only a single word for this function.");
                    }
                    else
                    {
                        Console.WriteLine(ReadTwistedWrod(input));
                    }
                    break;
                case "3":
                    Console.WriteLine("Please enter a phrase:");
                    input = Console.ReadLine();
                    Console.WriteLine("Twisted phrase: " + TwistPhrase(input));
                    break;
                case "4":
                    Console.WriteLine("Please enter a twisted phrase:");
                    input = Console.ReadLine();
                    Console.WriteLine(ReadTwistedPhrase(input));
                    break;
                case "5":
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Please only choose from the above options.");
                    break;
            }
            Main();
        }


        // We take a word and twist it
        public static string Twist(string input)
        {
            //Read user input
            StringBuilder userInput = new StringBuilder(input);

            //Get first and last chars
            char firstChar = userInput[0];
            int restLenght = userInput.Length - 1;
            char lastChar = userInput[restLenght];

            
            StringBuilder toBeTwisted = new StringBuilder();

            if (userInput.Length >= 4 && input[1] != input[2])
            {
                for (int i = 1; i < restLenght; i++)
                {
                    // Create string to be twisted
                    toBeTwisted.Append(userInput[i]);
                }

                Random r = new Random();

                // randomized string from https://www.tutorialspoint.com/randomize-string-in-chash
                string randomizedString = new string(toBeTwisted.ToString().ToCharArray().OrderBy(s => (r.Next(2) % 2) == 0).ToArray());
                return firstChar + randomizedString + lastChar;
            } else
            {
                // if it can't be twisted return the input
                return userInput.ToString();
            }
        }

        // twist words in a phrase
        private static string TwistPhrase(string input)
        {
            // split input string to words
            string[] words = input.Split(' ');
            string output = "";

            foreach (var word in words)
            {
                // for each word return twisted word
                output += Twist(word);
                output += " ";
            }
            return output;
        }

         // take a twisted word and return original
        private static string ReadTwistedWrod(string input)
        {
            // find first char, middle string and last char
            char firstChar = input[0];
            char lastChar = input[input.Length - 1];
            int middleLength = input.Length - 2;
            string myMiddle = input.Substring(1, middleLength);
            string output = input;
                        
            // word list
            string[] wordList = File.ReadAllLines(@"files\woerterliste.txt");

            foreach (var word in wordList)
            {
                // check if first char and last char of word match input and word and input have the same length
                if (word.Length == input.Length && word[0] == firstChar && word[word.Length -1] == lastChar)
                {
                    string wordMiddle = word.Substring(1, middleLength);
                    bool contained = true;
                    int counter = myMiddle.Length;
                    while(contained && counter > 0)
                    {
                        for(int i = 0; i < myMiddle.Length; i++)
                        {
                            // if chars of input are not contained I set contained to false
                            if (!wordMiddle.Contains(myMiddle[i]))
                            {
                                contained = false;
                            }
                            counter--;
                        }
                    }
                    // if contained is true after the while is done, I have the word
                    if(contained)
                    {
                        // assign output to the current word
                        output = word ;
                    }
                }
            }
            // return output
            return output;
        }

        private static string ReadTwistedPhrase(string input)
        {
            // take input and split it to words
            string[] words = input.Split(' ');
            // initialize output string
            string output = "";

            foreach (var word in words)
            {
                if(word.Length > 3)
                {
                    // if word lenght is more than 3 I untwist it
                    output += ReadTwistedWrod(word);
                    output += " ";
                } else
                {
                    // if word is smalled than 3 I return the word unchanged
                    output += word;
                    output += " ";
                }
            }

            // return final output
            return output;
        }
    }

}


